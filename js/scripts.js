$(document).ready(function () {
    $('#loginButton').button('toggle');
    $('#reserveButton').button('dispose');
    $('#loginButton').click(function () {
        $("#loginModal").modal('toggle');
    });
    $('#reserveButton').click(function () {
        $('#reserve').modal('toggle');
    });
    $('#mycarousel').carousel({ interval: 2000 });
    $('#carouselButtons').click(function () {
        if ($('#carouselButtons').children('span').hasClass('fa fa-pause')) {
            $('#mycarousel').carousel('pause');
            $('#carouselButtons').children('span').removeClass('fa fa-pause');
            $('#carouselButtons').children('span').addClass('fa fa-play');
        }
        else {
            $('#mycarousel').carousel('cycle');
            $('#carouselButtons').children('span').removeClass('fa fa-play');
            $('#carouselButtons').children('span').addClass('fa fa-pause');
        }
    });

});